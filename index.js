// This is used to get the contents of the express package to be used by the application
const express = require("express");

// Creates an application using express
// This creates an express application and stores it in a variable
// In layman's term, app is our server
const app = express();

// For our application server to run, we need a posrt to lsiten to
const port = 3000;

// Setup for allowing the server to handle data from requests
// Allows our app to read JSON data
// Middleware is a request handler that has access to the application's request and response cycle
app.use(express.json())


let users = [
	{
		username: "TStark3000",
		email: "starkindustries@mail.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker"
	}
];


// [SECTION] Routes


// express has methods corresponmding to each http method

// this route expects to receive a GET request at the base URI
// this will return a simple message back to the client
app.get("/", (req, res) => {

	// combines writeHead() and end()
	// it uses the express JS modules' metghod instead to send a response back to the client
	res.send("Hello World")
})

// MINI ACTIVITY
app.get("/greeting", (req, res) => {
	res.send("Hello from Batch244-Dionisio")
})

// this route expects to receive a POST request

app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

// this route expects to receive a post request at the URI  /signup
app.post("/signup", (req, res) => {

	if (req.body.username !== "" && req.body.password !== "" && req.body.email !== ""){
		users.push(req.body)
		res.send(`User ${req.body.username} is successfully registered!`)
	}
	else {
		res.send("Please input BOTH usename and password")
	}
})

//retrive all users


//update password (PUT) 
app.put("/change-password", (req, res) => {
	let message;

	for(let i = 0; i < users.length; i++){

		if (req.body.username == users[i].username){
			users[i].password = req.body.password

			message = `User ${req.body.username}'s password has been updated`
			break;
		} else {
			message = "User does not exist."
		}
		
	}
	res.send(message);
})


// ACTIVITY

// homepage
app.get("/home", (req, res) => {
	res.send("Welcome to the Homepage!")
})

// // retrive All User
app.get("/users", (req, res) => {
	res.send(users);
});

// // Delete user
app.delete("/delete-user", (req, res) => {
    let message;

    for(let i = 0; i < users.length; i++){

        if (req.body.username == users[i].username){
            users.splice(users.indexOf(req.body))

            message = `User ${req.body.username} has been deleted`
            break;
        } else {
            message = "User does not exist."
        }
    }
    res.send(message)
})


app.listen(port, () => console.log(`Server is running at port ${port}`))